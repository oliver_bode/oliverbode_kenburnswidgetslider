<?php
namespace Oliverbode\KenBurnsWidgetSlider\Block\Widget;

class Slider extends \Oliverbode\KenBurnsWidgetSlider\Block\Slider implements \Magento\Widget\Block\BlockInterface
{
    public function getTemplate()
    {
        if (is_null($this->_template)) {
            $this->_template = 'Oliverbode_KenBurnsWidgetSlider::slider.phtml';
        }
        return $this->_template;
    }
}
